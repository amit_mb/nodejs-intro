const http = require("http");

const server = http.createServer(function (req, res) {
  if (req.url === "/") {
    res.write("Welcome to home page");
    res.end();
  }
  if (req.url === "/about") {
    res.write("About page");
    res.end();
  }
});



server.listen(3000)